# READ ME

## Steps to deploy ECRF war file in a server.
Pre-Request
- install git bash
- install maven
- install java jdk 8

Clone the project from Git Repository
```bash             
git clone repository_url.git 
```

go to POM.xml file
```bash
nano  ./Ecrf/Pom.xml
```

Update Profile to respective to your server as shown below
```pom.xml 
<profile>
   <id>prod</id>
   <properties>
      <db.url>jdbc:postgresql://Aws-RDS-url:PostgresRDSPort/ecrfProdV1?useSSL=false</db.url>
      <db.username>enter-db_username</db.username>
      <db.password>enter-db_password</db.password>
      <ecrf.domain.login.url>https://ecgregistry.in/ecrfWeb/login/</ecrf.domain.login.url>
      <ecrf.domain.resetpassword.url>https://ecgregistry.in/ecrfWeb/auth/resetPassword/</ecrf.domain.resetpassword.url>
      <s3.bucket>enter-s3-bucket-name</s3.bucket>
      <s3.region>enter-type-of-region</s3.region>
      <s3.accesskey>paste-aws-access-key-here</s3.accesskey>
      <s3.secretkey>paste-aws-secret-key-here</s3.secretkey>
      <smtp.host>smtpout.secureserver.net</smtp.host>
      <smtp.username>enter-smtp-username</smtp.username>
      <smtp.password>enter-smtp-password</smtp.password>
   </properties>
</profile>
```
Note down the profile id name 

Save and exit the pom.xml
```bash
Ctrl+o 
```
```
Enter
```
```
Ctrl+x
```

After updating Pom.xml build the application using mvn cmd in root dir
```
admin@DESKTOP-TIHNO28 MINGW64 /d/Ecrf
$ ls
mvnw*  mvnw.cmd  pom.xml  src/ 

```
Enter maven command to build the application
```
mvn clean install -P<profilename> -DskipTests
```
